<?php

namespace Mihai\Utils;

class Strings
{
	public static function niceUrlTitle($string)
	{
		if (empty($string)) {
			return '';
		}

		$string = strtolower($string);
		$string = preg_replace('/[^a-zA-Z0-9\-]/', '-', $string);
		$string = preg_replace('/([\-]{2,})/', '-', $string);
		$string = trim($string, '-');

		return $string;
	}
}
