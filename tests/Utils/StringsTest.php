<?php

namespace tests\Utils;

use PHPUnit\Framework\TestCase;
use Mihai\Utils\Strings;

class StringsTest extends TestCase
{
	/**
	 * @dataProvider stringsProvider()
	 */
	public function test_nice_url_title($result, $value)
	{
		self::assertSame($result, Strings::niceUrlTitle($value));
	}

	public function stringsProvider()
	{
		return [
			['', ''],
			['my-computer-is-the-best', 'My computer is THE BEST'],
			['my-computer-is-the-best', 'My computer   is THE   BEST'],
			['my-operating-system-is-linux', 'My operating system is LINUX'],
			['tutorial-folosit-de-symfony-1234', 'Tutorial, folosit de Symfony 1234.'],
			['tutorial-folosit-de-symfony-1234', 'Tutorial/folosit#de$Symfony~1234.'],
		];
	}
}
